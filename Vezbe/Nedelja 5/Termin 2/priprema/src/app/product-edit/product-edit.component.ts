import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Product } from '../models/product';
import { ProductService } from '../services/product.service';

@Component({
  selector: 'app-product-edit',
  templateUrl: './product-edit.component.html',
  styleUrls: ['./product-edit.component.css']
})
export class ProductEditComponent implements OnInit {
  product: Product | null = null;
  constructor(private productService: ProductService, private activatedRoute: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    this.productService.getOne(this.activatedRoute.snapshot.params["id"]).subscribe(r => {
      this.product = r;
    });
  }

  update(product: Product) {
    this.productService.update(this.activatedRoute.snapshot.params["id"], product).subscribe(r => {
      this.router.navigate(["/products"]);
    });
  }

}
