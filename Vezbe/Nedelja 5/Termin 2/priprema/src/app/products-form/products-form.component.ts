import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Product } from '../models/product';
import { ProductService } from '../services/product.service';

@Component({
  selector: 'app-products-form',
  templateUrl: './products-form.component.html',
  styleUrls: ['./products-form.component.css']
})
export class ProductsFormComponent implements OnInit, OnChanges {
  @Input()
  product: Product|null = null;
  @Output()
  onSubmit: EventEmitter<Product> = new EventEmitter<Product>();

  forma: FormGroup = new FormGroup({
    "title": new FormControl(""),
    "description": new FormControl(""),
    "price": new FormControl("")
  })

  constructor() { }
  ngOnChanges(changes: SimpleChanges): void {
    
    if(changes["product"].currentValue != null) {
      this.forma.setValue({title: this.product.title, description: this.product.description, price: this.product.price});
    }
  }

  ngOnInit(): void {
  }

  submit() {
    this.onSubmit.emit(this.forma.value);
  }

}
