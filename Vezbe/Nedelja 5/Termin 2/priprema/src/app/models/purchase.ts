export interface Purchase {
    id?: number,
    quantity: number,
    price: number,
    productId: number,
    purchaseDate: string,
    completed: boolean
}