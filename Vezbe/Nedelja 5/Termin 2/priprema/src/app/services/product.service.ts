import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Product } from '../models/product';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private httpClient: HttpClient) { }

  getAll() {
    return this.httpClient.get<Product[]>("http://localhost:3000/products");
  }

  getOne(id: number) {
    return this.httpClient.get<Product>(`http://localhost:3000/products/${id}`);
  }

  create(product: Product) {
    return this.httpClient.post<Product>("http://localhost:3000/products", product);
  }

  update(id: number, product: Product) {
    return this.httpClient.put<Product>(`http://localhost:3000/products/${id}`, product);
  }

  delete(id: number) {
    return this.httpClient.delete<Product>(`http://localhost:3000/products/${id}`);
  }
}
