import { Component, OnInit } from '@angular/core';
import { Product } from '../models/product';
import { LoginService } from '../services/login.service';
import { ProductService } from '../services/product.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  products: Product[] = [];
  constructor(private productsService: ProductService, public loginService: LoginService) { }

  ngOnInit(): void {
    this.productsService.getAll().subscribe(r=>{
      this.products = r;
    })
  }

  delete(id:number) {
    this.productsService.delete(id).subscribe(r=>{
      this.productsService.getAll().subscribe(r=>{
        this.products = r;
      })
    });
  }

}
