import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';
import { LoginComponent } from './login/login.component';
import { ProductEditComponent } from './product-edit/product-edit.component';
import { ProductsComponent } from './products/products.component';

const routes: Routes = [
  {path: "login", component: LoginComponent},
  {path: "products", component: ProductsComponent, canActivate: [AuthGuard], data: {
    allowedRoles: ["ROLE_MANAGER", "ROLE_CUSTOMER"]
  }},
  {path: "products/:id", component: ProductEditComponent, canActivate: [AuthGuard], data: {
    allowedRoles: ["ROLE_MANAGER", "ROLE_CUSTOMER"]
  }}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
