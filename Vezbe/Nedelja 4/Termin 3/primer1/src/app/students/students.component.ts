import { Component, OnInit } from '@angular/core';
import { StudentsService } from '../services/students.service';

@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.css']
})
export class StudentsComponent implements OnInit {
  students = [];
  constructor(private studentsService : StudentsService) { }

  ngOnInit(): void {
    this.refresh();
  }

  refresh() {
    this.studentsService.getAll().subscribe(students => {
      this.students = students;
    })
  }

}
