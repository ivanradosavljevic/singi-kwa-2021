import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Student } from 'src/app/model/student';

@Component({
  selector: 'app-student-form',
  templateUrl: './student-form.component.html',
  styleUrls: ['./student-form.component.css']
})
export class StudentFormComponent implements OnInit, OnChanges {
  @Input()
  student: Student|null = null;

  @Output()
  public createEvent: EventEmitter<any> = new EventEmitter<any>();

  form: FormGroup = new FormGroup({
    id: new FormControl(null, [Validators.required]),
    ime: new FormControl(null, [Validators.required]),
    prezime: new FormControl(null, [Validators.required]),
    brojIndeksa: new FormControl(null, [Validators.required])
  });
  constructor() { }

  ngOnChanges(changes: SimpleChanges): void {
    console.log(changes);
    console.log(this.student);
    this.form.get("id")?.setValue(this.student?.id);
    this.form.get("ime")?.setValue(this.student?.ime);
    this.form.get("prezime")?.setValue(this.student?.prezime);
    this.form.get("brojIndeksa")?.setValue(this.student?.brojIndeksa);
  }

  ngOnInit(): void {
    this.form.get("id")?.setValue(this.student?.id);
    this.form.get("ime")?.setValue(this.student?.id);
    this.form.get("prezime")?.setValue(this.student?.id);
    this.form.get("brojIndeksa")?.setValue(this.student?.id);
  }

  create() {
    if(this.form.valid) {
      this.createEvent.emit(this.form.value);
    }
  }

}
