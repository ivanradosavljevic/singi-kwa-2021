export interface Student {
    id?: number,
    ime: string,
    prezime: string,
    brojIndeksa: string
}
