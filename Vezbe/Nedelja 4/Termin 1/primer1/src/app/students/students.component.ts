import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Student } from '../model/student';
import { StudentService } from '../services/student.service';

@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.css']
})
export class StudentsComponent implements OnInit {
  studenti: Student[] = []
  student: Student = { ime: "", prezime: "", brojIndeksa: "" }
  studentUpdate: Student = { ime: "", prezime: "", brojIndeksa: "" }
  studentUpdateId: number | null = null;

  constructor(private studentService: StudentService) { }

  ngOnInit(): void {
    this.getAll();
  }

  getAll() {
    this.studentService.getAll().subscribe((value) => {
      this.studenti = value;
    }, (error) => {
      console.log(error);
    });
  }

  delete(id: any) {
    this.studentService.delete(id).subscribe((value) => {
      this.getAll();
    }, (error) => {
      console.log(error);
    })
  }

  create() {
    this.studentService.create(this.student).subscribe((value) => {
      this.getAll();
    }, (error) => {
      console.log(error);
    })
  }

  update() {
    if (this.studentUpdateId) {
      this.studentService.update(this.studentUpdateId, this.studentUpdate).subscribe((value) => {
        this.getAll();
      }, (error) => {
        console.log(error);
      })
    }
  }

  setUpdate(student: any) {
    this.studentUpdateId = student.id;
    this.studentUpdate = { ...student };
  }

}
