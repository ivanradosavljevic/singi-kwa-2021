# Vežbe 1
*Cilj vežbi: Podsećanje HTML, CSS, JS.*

## Zadaci
1. Napraviti klijentsku web aplikaciju koja obezbeđuje interfejs za CRUD operacije nad studentima. Svaki student je opisan brojem indeksa, imenom, prezimenom, godinom upisa, smerom i prosečnom ocenom. Studente prikazati tabelarno. Unos podataka omogućiti preko forme u koju je neophodno uneti obavezne podatke: broj indeksa, ime, prezime, godina upisa i smer. U tabeli prikazati sve unete podatke, za podatke koji nisu uneti prikazati tekst "-".
2. Proširiti aplikaciju uvođenjem mogućnosti za pretragu studenata po svim atributima, pri čemu se pretraga po prosečnoj oceni vrši za zadati opseg ocena.
3. Proširiti aplikaciju uvođenjem entiteta smer, smer je opisan šifrom smera i nazivom. Omogućiti sve CRUD operacije za smer.
4. Prepraviti unos studenata tako da se smer bira iz padajuće liste prethodno unetih smerova.