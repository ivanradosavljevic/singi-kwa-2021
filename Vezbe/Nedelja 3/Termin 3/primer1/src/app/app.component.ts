import { Component } from '@angular/core';
import { Transakcija } from './model/transakcija';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'primer1';
  transakcijaZaIzmenu: Transakcija|null = null;

  izmeni(transakcija: Transakcija) {
    this.transakcijaZaIzmenu = transakcija;
  }
}
