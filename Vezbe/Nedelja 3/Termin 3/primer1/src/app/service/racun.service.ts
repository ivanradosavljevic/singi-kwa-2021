import { Injectable } from '@angular/core';
import { Racun } from '../model/racun';

@Injectable({
  providedIn: 'root'
})
export class RacunService {
  private racuni: Racun[] = [
    {id: 1, brojRacuna: "0012345600", stanje: 50000},
    {id: 2, brojRacuna: "0212345630", stanje: 55000},
  ];
  constructor() { }

  getAll() {
    return this.racuni;
  }

  getOne(id: number) {
    for(let k of this.racuni) {
      if(k.id == id) {
        return k;
      }
    }
    return null;
  }

  create(racun: Racun) {
    this.racuni.push(racun);
  }

  update(id: number, racun: Racun) {
    for(let i = 0; i < this.racuni.length; i++) {
      if(this.racuni[i].id == id) {
        this.racuni[i] = racun;
        return true;
      }
    }
    return false;
  }

  delete(id: number) {
    for(let i = 0; i < this.racuni.length; i++) {
      if(this.racuni[i].id == id) {
        this.racuni.splice(i, 1);
        return true;
      }
    }
    return false;
  }
}
