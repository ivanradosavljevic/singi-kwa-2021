import { Injectable } from '@angular/core';
import { Klijent } from '../model/klijent';

@Injectable({
  providedIn: 'root'
})
export class KlijentService {
  private klijenti: Klijent[] = [
    {id: 1, ime: "Pera", prezime: "Perić"},
    {id: 2, ime: "Marko", prezime: "Marković"},
  ];

  constructor() { }

  getAll() {
    return this.klijenti;
  }

  getOne(id: number) {
    for(let k of this.klijenti) {
      if(k.id == id) {
        return k;
      }
    }
    return null;
  }

  create(klijent: Klijent) {
    this.klijenti.push(klijent);
  }

  update(id: number, klijent: Klijent) {
    for(let i = 0; i < this.klijenti.length; i++) {
      if(this.klijenti[i].id == id) {
        this.klijenti[i] = klijent;
        return true;
      }
    }
    return false;
  }

  delete(id: number) {
    for(let i = 0; i < this.klijenti.length; i++) {
      if(this.klijenti[i].id == id) {
        this.klijenti.splice(i, 1);
        return true;
      }
    }
    return false;
  }
}
