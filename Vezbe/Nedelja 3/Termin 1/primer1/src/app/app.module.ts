import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SmeroviComponent } from './smerovi/smerovi.component';
import { DodavanjeSmeraComponent } from './smerovi/dodavanje-smera/dodavanje-smera.component';
import { IzmenaSmeraComponent } from './smerovi/izmena-smera/izmena-smera.component';

@NgModule({
  declarations: [
    AppComponent,
    SmeroviComponent,
    DodavanjeSmeraComponent,
    IzmenaSmeraComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
