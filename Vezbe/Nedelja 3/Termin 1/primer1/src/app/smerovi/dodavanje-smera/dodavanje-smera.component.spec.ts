import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DodavanjeSmeraComponent } from './dodavanje-smera.component';

describe('DodavanjeSmeraComponent', () => {
  let component: DodavanjeSmeraComponent;
  let fixture: ComponentFixture<DodavanjeSmeraComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DodavanjeSmeraComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DodavanjeSmeraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
