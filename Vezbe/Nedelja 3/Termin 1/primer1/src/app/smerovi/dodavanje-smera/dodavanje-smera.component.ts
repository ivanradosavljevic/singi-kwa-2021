import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-dodavanje-smera',
  templateUrl: './dodavanje-smera.component.html',
  styleUrls: ['./dodavanje-smera.component.css']
})
export class DodavanjeSmeraComponent implements OnInit {
  smer = {
    "sifra": "",
    "naziv": ""
  }

  @Output()
  dodavanjeSmera:EventEmitter<any> = new EventEmitter<any>();

  constructor() { }

  ngOnInit(): void {
  }

  dodaj() {
    this.dodavanjeSmera.emit({...this.smer});
  }

}
