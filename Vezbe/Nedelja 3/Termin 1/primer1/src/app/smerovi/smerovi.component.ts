import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-smerovi',
  templateUrl: './smerovi.component.html',
  styleUrls: ['./smerovi.component.css']
})
export class SmeroviComponent implements OnInit {
  @Input()
  smerovi:any[] = [];

  @Output()
  ukloniRed:EventEmitter<number> = new EventEmitter<number>();
  @Output()
  izmeniRed:EventEmitter<any> = new EventEmitter<any>();

  @Output()
  dodajRed:EventEmitter<any> = new EventEmitter<any>();


  constructor() { }

  ngOnInit(): void {
  }

  ukloni(index:number) {
    this.ukloniRed.emit(index);
  }

  dodaj(smer:any) {
    this.dodajRed.emit({...smer});
  }

  izmeni(indeks:number, smer:any) {
    this.izmeniRed.emit({indeks, ...smer});
  }
}
