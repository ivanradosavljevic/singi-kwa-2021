import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IzmenaSmeraComponent } from './izmena-smera.component';

describe('IzmenaSmeraComponent', () => {
  let component: IzmenaSmeraComponent;
  let fixture: ComponentFixture<IzmenaSmeraComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IzmenaSmeraComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IzmenaSmeraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
