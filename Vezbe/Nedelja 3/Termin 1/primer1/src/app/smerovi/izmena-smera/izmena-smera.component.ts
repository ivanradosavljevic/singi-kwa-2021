import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-izmena-smera',
  templateUrl: './izmena-smera.component.html',
  styleUrls: ['./izmena-smera.component.css']
})
export class IzmenaSmeraComponent implements OnInit {
  @Input()
  smer = {
    "indeks": "",
    "sifra": "",
    "naziv": ""
  }

  @Output()
  izmenaSmera:EventEmitter<any> = new EventEmitter<any>();

  constructor() { }

  ngOnInit(): void {
  }

  izmeni() {
    this.izmenaSmera.emit({...this.smer});
  }
}
