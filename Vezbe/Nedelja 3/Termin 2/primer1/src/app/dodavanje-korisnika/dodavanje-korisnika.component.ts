import { Component, OnInit } from '@angular/core';
import { Korisnik } from '../model/korisnik';
import { KorisnikService } from '../services/korisnik.service';

@Component({
  selector: 'app-dodavanje-korisnika',
  templateUrl: './dodavanje-korisnika.component.html',
  styleUrls: ['./dodavanje-korisnika.component.css']
})
export class DodavanjeKorisnikaComponent implements OnInit {
  korisnik: Korisnik = {
    id: 0,
    korisnickoIme: "",
    email: "",
    lozinka: ""
    };

  constructor(private korisniciServis: KorisnikService) { }

  ngOnInit(): void {
  }

  dodaj() {
    this.korisniciServis.insert({...this.korisnik});
  }
}
