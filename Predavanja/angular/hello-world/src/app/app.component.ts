import { Component } from "@angular/core";
import { Person } from './model/person'



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'dobar dan';
  brojac = 0;
  ime = 'Pera';
  prezime = 'Petrovic';
  plata = 100.34;

  person = new Person();


  username = "";
  password = "";
  message = "";

  login(){
    if(this.username=="root" && this.password=="1234"){
      this.message = "OK";
    }else{
      this.message = "Error";
    }
  }

  reakcijaNaDogadjaj(a:String){
    console.log(a);
  }

  povecaj(){
    this.brojac++;
  }

  round(vrednost:Number):Number{
    return vrednost;
  }
}
