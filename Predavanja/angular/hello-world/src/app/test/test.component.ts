import { Component, Input, Output, OnInit, EventEmitter} from '@angular/core';
import { Person } from '../model/person'

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit {

  @Input('parentPerson') public person:Person;

  @Output() _event = new EventEmitter<String>();

  nesto:string;
  constructor() { 
    this.nesto = 'nesto';
    this.person = new Person();
  }

  ngOnInit(): void {
  }

  akcija(){
    this.person.username = "Promena u akciji";
    this._event.emit('AKCIJA');
  }

}
