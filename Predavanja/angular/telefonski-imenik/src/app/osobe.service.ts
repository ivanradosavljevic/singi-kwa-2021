import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
// import { Observable } from 'rxjs/Observable';
// import 'rxjs/add/operator/catch';
// import 'rxjs/add/observable/throw';

import {Osoba} from './osoba';

@Injectable({
  providedIn: 'root'
})
export class OsobeService {

  public osobe:Array<Osoba>;

  constructor(private http:HttpClient) { 
    this.osobe = new Array<Osoba>()
  }

  public add(osoba:Osoba){
    this.http.get('/osobe').then(response=>this.osobe = response.data)
    this.osobe.push(osoba);
  }
}
