import { TestBed } from '@angular/core/testing';

import { OsobeService } from './osobe.service';

describe('OsobeService', () => {
  let service: OsobeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(OsobeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
