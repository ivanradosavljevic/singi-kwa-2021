import { Component } from '@angular/core';
import {Osoba} from './osoba'
import { OsobeService } from './osobe.service';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'telefonski-imenik';
  brojac = 0;

  uvecaj(){
    this.brojac+=1;
  }

  gradovi = ['Novi Sad', 'Beograd', 'Subotica', 'Pancevo'];

  osoba:Osoba;
  

  constructor(private osobeService:OsobeService) { 
    this.osoba = new Osoba();
    
  }

  dodajOsobu(){
    this.osobeService.add(this.osoba);
    this.osoba = new Osoba();

  }



}
