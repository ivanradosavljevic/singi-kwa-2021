import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
    {{title}}

    <app-employee-list [param1]="title" (_event)="reakcija($event)"></app-employee-list>
    <app-employee-details ></app-employee-details>

  `,
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'test-app';

  reakcija(a:string){
    this.title = a;
  }

}
