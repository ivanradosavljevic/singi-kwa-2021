import { Component, Input, OnInit } from '@angular/core';
import { EmployeeService } from '../employee.service';

@Component({
  selector: 'app-employee-details',
  template: `
    <h1>Employee details</h1>
    <p *ngFor="let employee of employees">
      {{employee.id}} - {{employee.name}} - {{employee.age}}
    </p>
  `,
  styleUrls: ['./employee-details.component.css']
})
export class EmployeeDetailsComponent implements OnInit {

  public employees = []

  constructor(private service:EmployeeService){
    service.getEmployees().subscribe(data=>this.employees=data);
  }

  ngOnInit(): void {
  }

}
