import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';
import { EmployeeService } from '../employee.service';

@Component({
  selector: 'app-employee-list',
  template: `
    <h1>Employee list</h1> Param1: {{param1}}
    <p *ngFor="let employee of employees">
      {{employee.name}}
    </p>
    <a href="#" (click)="add()">Add</a>

    <a href="#" (click)="dogadjaj()">Promeni parent title</a>
  `,
  styleUrls: ['./employee-list.component.css']
})
export class EmployeeListComponent implements OnInit {

  @Input() param1 = "";

  @Output() _event = new EventEmitter<String>();

  public employees = []

  constructor(private service:EmployeeService){
    service.getEmployees().subscribe(data=>this.employees=data);
    // this.data = service.getEmployees();
  }

  add(){
    this.service.add({
      id:10,
      name: "Robert",
      age: 45
    })
  }

  dogadjaj(){
    this._event.emit('Novi naslov u parent komponenti');
  }
   
  ngOnInit(): void {
  }

}
