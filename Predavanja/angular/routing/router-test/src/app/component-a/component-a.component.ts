import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CcComponent } from '../d/cc/cc.component';

@Component({
  selector: 'app-component-a',
  templateUrl: './component-a.component.html',
  styleUrls: ['./component-a.component.css']
})
export class ComponentAComponent implements OnInit {

  id = '13';

  constructor(private route: ActivatedRoute, private router: Router,) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('id');

  }

}
