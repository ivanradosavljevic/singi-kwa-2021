from flask import Flask, render_template, request, redirect
import json
from flask_cors import CORS

app = Flask(__name__, static_folder="static", static_url_path="/")
CORS(app)

@app.route("/api/colors")
def colors():
    colors = ['red', 'green', 'blue']
    return json.dumps(colors)

@app.route("/api/students")
def students():
    ret = [
        {"ime":"Pera", "prezime":"Tesla"},
        {"ime":"Marko", "prezime":"Pupin"},
        {"ime":"Milica", "prezime":"Milinkovic"},
        {"ime":"Djordje", "prezime":"Petrovic"},
    ]
    return json.dumps(ret)


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8081, debug=True, )